from flask import Flask, request, jsonify
from payment_processing_schema import PaymentProcessingSchema
from payment_gateway_transaction import PaymentGatewayTransaction
from marshmallow import ValidationError
import json

app = Flask(__name__)

@app.route('/process-payment', methods=["POST"])
def ProcessPayment():
	"""Processes payments based on the """
	payment_req = request.get_json()

	try:
		payment_processing_schema = PaymentProcessingSchema()
		payment_processing_schema.loads(json.dumps(payment_req))
	except ValidationError as err:
		return jsonify(
			message="The request is invalid",
			status=400
		)

	try:
		pg_obj = PaymentGatewayTransaction()
		payment_successful = pg_obj.handle_payment_gateway_transaction(payment_req.get("Amount"))
		if payment_successful:
			return jsonify(
				message="Payment is processed",
				status=200
			)
	except Exception as e:
		return jsonify(
		message=e,
		status=500
	)

#Testing Payment Gateway
@app.route('/CheapPaymentGateway', methods=["POST"])
def cheap_payment_gateway():
	return jsonify(
		message="Payment is processed",
		status=200
	)

if __name__ == '__main__':
	app.run(debug=True)