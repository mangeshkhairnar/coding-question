from marshmallow import ValidationError
from urllib3 import PoolManager, util, exceptions
import json

class PaymentGatewayTransaction:

	def send_payment_gateway_request(self, payment_gateway, retry=0):
		"""Send a request to the payment gateway"""
		RETRY_CODES = [429, 502, 503]
		try:
			retries = util.Retry(total=retry, backoff_factor=1, status_forcelist=RETRY_CODES)
			http = PoolManager(retries=retries)
			response = http.request('POST', "http://localhost:5000/{}".format(payment_gateway))

			if json.loads(response.data)["status"] == 200:
				return True
		except exceptions.HTTPError as exc:
			# if the error is not retryable, re-raise the exception
			if response.status_code not in RETRY_CODES:
				raise exc
			else:
				handle_failure(payment_gateway)

	def handle_failure(self, payment_gateway):
		if payment_gateway == "ExpensivePaymentGateway":
			return self.send_payment_gateway_request("CheapPaymentGateway")
		else:
			raise exc

	def handle_payment_gateway_transaction(self, amount):
		"""
		Compare the amount given with the threshold and
		return the payment gateway based on the threshold.
		
		Args:
		amount: amount to be processed in float

		Return:
		return the payment gateway
		"""
		if 0 < amount < 20:
			return self.send_payment_gateway_request("CheapPaymentGateway", retry=False)
		elif 20 <= amount <=500:
			return self.send_payment_gateway_request("ExpensivePaymentGateway", retry=False)
		elif amount > 500:

			return self.send_payment_gateway_request("PremiumPaymentGateway", retry=3)
		else:
			raise ValidationError()