import unittest
import json
from datetime import datetime

from app import app

class PaymentProcessingTest(unittest.TestCase):
	def setUp(self):
		self.app = app.test_client()

	def test_payment_schema(self):
		# Given
		payload = json.dumps({
			"CreditCardNumber": "4111 1111 1111 111",
			"CardHolder": "Test User",
			"ExpirationDate": datetime.now(),
			"SecurityCode": "123",
			"Amount": 132.50
		}, default=str)

		# Send a post request to the api
		response = self.app.post('/process-payment', headers={"Content-Type": "application/json"}, data=payload)

		self.assertEqual("The request is invalid", response.json['message'])
		self.assertEqual(400, response.json['status'])

	def test_payment_processing(self):
		payload = json.dumps({
			"CreditCardNumber": "234 567 891 234",
			"CardHolder": "Test User",
			"ExpirationDate": datetime.now(),
			"SecurityCode": "123",
			"Amount": 12.50
		}, default=str)

		# Send a post request to the api
		response = self.app.post('/process-payment', headers={"Content-Type": "application/json"}, data=payload)

		self.assertEqual("Payment is processed", response.json['message'])
		self.assertEqual(200, response.json['status'])
