from marshmallow import Schema, fields, validates, ValidationError
from datetime import datetime
from credit_card_checker import CreditCardChecker

class PaymentProcessingSchema(Schema):
	CreditCardNumber = fields.String(required=True)
	CardHolder = fields.String(required=True)
	ExpirationDate = fields.DateTime(required=True)
	SecurityCode = fields.String()
	Amount = fields.Float(required=True)

	@validates("CreditCardNumber")
	def validate_credit_card_number(self, cc_number):
		"""Validate the credit card number with a specific regex."""
		if not CreditCardChecker(cc_number).valid():
			raise ValidationError("Invalid Credit Card Number")
	
	@validates("ExpirationDate")
	def validate_expiration(self, datetime):
		"""Validate the expiration date provided by comparing it with the now date."""
		now = datetime.now()
		if now < datetime:
			raise ValidationError("Datetime cannot be in the past")

	@validates("SecurityCode")
	def validate_security_code(self, sec_code):
		"""Validate if a security code is 3 characters long."""
		if len(sec_code) != 3:
			raise ValidationError("Invalid security code")

	@validates("Amount")
	def validate_amount(self, amt):
		"""Validate if the amount is positive."""
		if amt <=0:
			raise ValidationError("Amount must not be negative")